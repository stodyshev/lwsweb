import { sampleConfigs, sampleDevices } from "./sampleData";

export async function fetchConfigurations() {
  // TODO replace with API call fetching configs from LWS server
  return sampleConfigs;
}

export async function fetchDevices() {
  return sampleDevices;
}
