## TODO

Some things todo in priority order

- [x] use next.js
- [x] keep it simple stupid!
- [x] dashboard with links to configs, logs, devices, etc
- [x] simple navigation, breadcrumbs on each page, page template
- [ ] device manager client

  - [x] typed model
  - [x] quick prototype:
    - [x] XML to Model converter
    - [x] render config tree with icons
    - [x] configs/devices pages
    - [x] render device tree
    - [x] split page with dynamic right view
    - [ ] tree/list views for configs and devices
    - [x] for interface node render device tree on the right side
      - [x] extract device tree
      - [ ] filter by compatible interfaces
    - [ ] actions
      - [ ] unlink interface
      - [ ] link to device interface
      - [ ] create/save configuration
    - [ ] initial integration with server
      - [x] deal with TCP to HTTP requests conversion
      - [ ] API is command based
      - [ ] need to understand XML API, request code of desktop clients
    - [ ] required/optional device interfaces
    - [ ] render additional markers to show incomplete configuration with missing links
    - [ ] keyboard navigation in device tree
    - [x] simple settings tree without validation/restrictions
      - [x] ACE based JSON editor https://github.com/josdejong/jsoneditor
      - [x] https://github.com/mac-s-g/react-json-view/
      - [ ] https://github.com/ValYouW/jqPropertyGrid - simple property grid like this one

- [ ] tooling
  - [ ] CSS modules
  - [ ] prettier, auto format code on git commit
- [ ] consider mongodb/postgresql powered JSON API
  - [ ] CRUD configs
  - [ ] XML to JSON conversion on server
- [x] better navigation
  - [x] sidebar navigation
  - [ ] filter sidebar (search input, etc)
- [ ] simple dashboard
  - [ ] number of configurations
  - [ ] number of devices
- [ ] just for fun
  - [ ] dynamic icons for devices
  - [x] theme switcher
  - [x] scroll to top (take from antd admin)
- [x] refactoring
  - [x] theme context
  - [x] isMobile context

## IDEAS

- JSON API is really desired
- each device can have own page also
- e.g. it is easy to work with detectors
