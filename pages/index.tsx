import dynamic from "next/dynamic";
import Page from "../components/Page";
import Center from '../components/Center';

const Snowflakes = dynamic(() => import("../components/Snowflakes"), { ssr: false });

function HomePage() {
  return (
    <Page>
      <Snowflakes>
        <Center>
          <h1 style={{ fontSize: 48, textAlign: "center", color: "white" }}>Welcome to LWSWEB!</h1>
        </Center>
      </Snowflakes>
    </Page>
  );
}

export default HomePage;
