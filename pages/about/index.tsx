import Page from "../../components/Page";

export default function About() {
  return (
    <Page>
      <h1>About LWSWEB</h1>
      <p>This is web interface to manage resources of LWSBOX.</p>
      <p>
        It is built using <a href="https://nextjs.org/">next.js</a>
      </p>
    </Page>
  );
}
