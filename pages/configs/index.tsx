import _ from "lodash";
import dynamic from "next/dynamic";
import { Tree } from "antd";
import SplitPane from "react-split-pane";
import useSWR from "swr";
import Page from "../../components/Page";
import ConfigNode from "../../components/ConfigNode";
import DeviceTree from "../../components/DeviceTree";
import { fetchConfigurations } from "../../api";
import { useState } from "react";

const JsonView = dynamic(() => import("react-json-view"), { ssr: false });

// This page renders configurations as tree
export default function Configurations() {
  const { data, error } = useSWR("/configs", fetchConfigurations);
  const [selectedItem, setSelectedItem] = useState({
    type: "unknown",
    value: undefined
  });

  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  const nodes = data.map(ConfigNode);

  const findDevice = (name: string) =>
    _.flatMap(data.map(c => c.devices)).find(d => d.name === name);

  const onSelect = (selectedKeys, info) => {
    const moniker = _.isEmpty(selectedKeys) ? [] : selectedKeys[0].split("-");
    const type = moniker[0];
    const name = moniker[1];
    switch (type) {
      case "config":
        const config = data.find(t => t.name === name);
        setSelectedItem({ type, value: config });
        break;
      case "device": {
        const device = findDevice(name);
        setSelectedItem({ type, value: device });
        break;
      }
      case "settings": {
        const device = findDevice(name);
        setSelectedItem({ type, value: device.settings });
        break;
      }
      case "interface": {
        const device = findDevice(name);
        const iface = device.deviceInterfaces.find(t => t.name === moniker[2]);
        setSelectedItem({
          type,
          value: {
            device,
            interface: iface
          }
        });
        break;
      }
      default:
        setSelectedItem({ type: "unknown", value: undefined });
        break;
    }
  };

  let view = undefined;
  switch (selectedItem.type) {
    case "config":
      // TODO render markdown description
      view = (
        <div>
          <h1>{selectedItem.value.name}</h1>
          <p>{selectedItem.value.description}</p>
        </div>
      );
      break;
    case "interface":
      view = <DeviceTree except={selectedItem.value.device.name} noSettings />;
      break;
    default:
      if (selectedItem.value) {
        view = <JsonView src={selectedItem.value} />;
      }
      break;
  }

  return (
    <Page>
      <SplitPane
        split="vertical"
        minSize={250}
        defaultSize={500}
        style={{ overflow: "auto" }}
        pane1Style={{ overflow: "auto" }}
        pane2Style={{ padding: "0 10px" }}
      >
        <div className="left-pane">
          <Tree
            className="dev-tree"
            showLine
            showIcon
            checkable={false}
            selectable
            onSelect={onSelect}
          >
            {nodes}
          </Tree>
        </div>
        <div className="right-pane">{view}</div>
      </SplitPane>
    </Page>
  );
}
