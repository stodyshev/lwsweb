import Page from "../../components/Page";
import DeviceTree from "../../components/DeviceTree";

export default function DevicesPage() {
  return (
    <Page>
      <DeviceTree />
    </Page>
  );
}
