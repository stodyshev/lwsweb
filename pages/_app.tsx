import React from "react";
import { ThemeProvider } from "../components/ThemeContext";
import { MobileProvider } from "../components/MobileContext";
import "antd/dist/antd.less";
import "../styles.less";

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider>
      <MobileProvider>
        <Component {...pageProps} />
      </MobileProvider>
    </ThemeProvider>
  );
}
