# lwsweb

Simple Web Interface to manage LWS resources

## How to run

You need the following tools to be installed on your system:

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [nodejs](https://nodejs.org/) LTS version
- [yarn](https://yarnpkg.com/) package manager

git clone https://bitbucket.org/stodyshev/lwsweb/

Inside the repo run:

- `yarn install`
- `yarn dev`
- browse http://localhost:3000
