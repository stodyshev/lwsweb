import _ from "lodash";
import convert from "xml-js";
import { Configuration, Device } from "./types";

// this is real config from Ruslan
const xml = `
<config name="CLWS300 ver201 PMD301">
  <device name="CLWS ver 2.01">
    <interfaces>
      <interface name="X.Coordinate" device="LWM Emulator" interface="X Coordinate"/>
      <interface name="X.DAQ" device="LWM Emulator" interface="Coordinate X Data Acquisition System"/>
      <interface name="Y.Coordinate" device="PMD301" interface="Piezo Motor coordinate (close loop)"/>
      <interface name="Y.Current Detector" device="PMD301" interface="Piezo Motor coordinate (close loop)"/>
      <interface name="Angular.Coordinate" device="LWM Emulator" interface="Angle Coordinate"/>
      <interface name="AutoFocus.Coordinate" device="LWM Emulator" interface="Z Coordinate"/>
      <interface name="AutoFocus.Laser Switch" device="LWM Emulator" interface="Switch"/>
      <interface name="Sync.Settings" device="LWM Emulator" interface="Sync Object"/>
      <interface name="Sync.ZL Detector" device="LWM Emulator" interface="Power Detector"/>
      <interface name="Sync.Synchro Clock Detector" device="LWM Emulator" interface="Synchroimpulse Detector"/>
      <interface name="Sync DAQ.Data Acquisition" device="LWM Emulator" interface="Data Acquisition System"/>
      <interface name="Sync DAQ.Pattern Generator" device="LWM Emulator" interface="Sync Pattern Generator"/>
      <interface name="Feedback.Regulator" device="LWM Emulator" interface="Laser Power Regulator"/>
      <interface name="AOM2.Output" device="LWM Emulator" interface="Three Position Switch"/>
      <interface name="AOM2.Amplifier" device="LWM Emulator" interface="Modulator Amplifier"/>
      <interface name="AOM2.Source" device="LWM Emulator" interface="Modulator Source"/>
      <interface name="AOM2.Pattern Generator" device="LWM Emulator" interface="Modulator Pattern Generator"/>
      <interface name="Corrector.Generator" device="LWM Emulator" interface="Corrector Pattern Generator"/>
      <interface name="Corrector.Amplifier" device="LWM Emulator" interface="Corrector Amplifier"/>
      <interface name="Switch.Gate" device="LWM Emulator" interface="Switch"/>
      <interface name="Photo Detector.Feedback" device="LWM Emulator" interface="Power Detector"/>
      <interface name="Photo Detector.Calibration" device="LWM Emulator" interface="Power Detector"/>
      <interface name="Photo Detector.Reflection" device="LWM Emulator" interface="Power Detector"/>
      <interface name="Detector.Aux 1" device="PMD301" interface="Piezo Motor coordinate (close loop)"/>
      <interface name="Detector.Aux 2" device="CHR150" interface="Distance detector"/>
      <interface name="Detector.Aux 3" device="CHR150" interface="Inverse Distance detector"/>
      <interface name="Angular Amplifier.ON Switch" device="LWM Emulator" interface="Switch"/>
      <interface name="X Coordinate Amplifier.ON Switch" device="LWM Emulator" interface="Switch"/>
    </interfaces>
  </device>
  <device name="LWM Emulator">
    <settings>
      <Test1 Test1_1="true" Test1_2="true"/>
      <Test2 Test2_1="false"/>
    </settings>
  </device>
  <device name="PMD301">
    <settings>
      <Connection>
        <int name="COM port" value="3"/>
      </Connection>
      <Main>
        <int name="Control type" value="0"/>
      </Main>
      <node name="Close loop" Auto-configuration="false">
        <int name="Min limit" value="-10000000"/>
        <int name="Max limit" value="10000000"/>
        <int name="Dead band" value="15"/>
        <bool name="Encoder counting direction" value="true"/>
        <int name="Steps per count" value="30"/>
        <double name="Encoder coeficient" value="0.005"/>
        <int name="Min speed" value="1"/>
        <int name="Max speed" value="100"/>
        <bool name="Auto OFF" value="true"/>
        <double name="Auto OFF delay" value="5.0"/>
      </node>
      <node name="Open loop">
        <int name="Move speed" value="100"/>
      </node>
    </settings>
  </device>
  <device name="CHR150">
    <settings>
      <Connection>
        <int name="COM port" value="1"/>
      </Connection>
    </settings>
  </device>
</config>`;

// convert XML string to JSON object
const root = JSON.parse(convert.xml2json(xml, { compact: true }));

function isVirtual(d: any) {
  return d.interfaces && !_.isEmpty(d.interfaces.interface);
}

// converts JSON XML tree to typed Configuration model
function convertConfig(root: any) {
  return {
    name: root.config._attributes.name,
    devices: root.config.device.map(d => ({
      name: d._attributes.name,
      kind: isVirtual ? "virtual" : "hard",
      settings: d.settings,
      deviceInterfaces: _.map(_.get(d, "interfaces.interface"), di => ({
        name: di._attributes.name,
        interfaces: [],
        link:
          di._attributes.device || di._attributes.interface
            ? {
                device: di._attributes.device,
                interface: di._attributes.interface
              }
            : undefined
      }))
    }))
  };
}

export const sampleConfigs: Configuration[] = [convertConfig(root)];
export const sampleDevices: Device[] = _.flatMap(sampleConfigs, c => c.devices);
