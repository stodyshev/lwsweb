import icons from "./components/icons";

export default [
  { href: "/", title: "Dashboard", icon: icons.dashboard },
  { href: "/configs", title: "Configurations", icon: icons.config },
  { href: "/devices", title: "Devices", icon: icons.device },
  { href: "/about", title: "About", icon: icons.info }
];
