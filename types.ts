export type Configuration = {
  name: string;
  description?: string;
  version?: string;
  devices: Device[];
};

export type Device = {
  name: string;
  kind: "virtual" | "hard";
  deviceInterfaces: DeviceInterface[];
  settings?: any;
};

export type InterfaceLink = {
  device: string;
  interface: string;
};

export type DeviceInterface = {
  name: string;
  interfaces: string[];
  // only for virtual devices
  link: InterfaceLink;
  optional?: boolean;
};

// misc types
export type Theme = "light" | "dark";

export type MenuItem = {
  id: string;
  route: string;
  name: string;
  icon: any;
};
