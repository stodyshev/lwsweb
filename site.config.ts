import routes from "./routes";
import { MenuItem } from "./types";

const menus: MenuItem[] = routes.map(t => ({
  id: t.href,
  route: t.href,
  name: t.title,
  icon: t.icon
}));

export default {
  logoPath: "/logo.svg",
  siteName: "LWSWEB",
  fixedHeader: true,
  menus
};
