import React, { useContext } from "react";
import Footer from "rc-footer";
import { ThemeContext } from "./ThemeContext";
import "rc-footer/assets/index.less";
import "./Footer.less";

export default function PageFooter() {
  const { theme } = useContext(ThemeContext);
  const copyright = <span>© 2020 IAE</span>;
  return (
    <Footer
      theme={theme}
      columns={[
        {
          icon: (
            <img src="https://gw.alipayobjects.com/zos/rmsportal/XuVpGqBFxXplzvLjJBZB.svg" />
          ),
          title: "LWSWEB"
        }
      ]}
      bottom={copyright}
    ></Footer>
  );
}
