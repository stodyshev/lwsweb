import React, { Fragment, useContext, useState } from "react";
import { Menu } from "antd";
import Icon from "./Icon";
import Link from "next/link";
import { useRouter } from "next/router";
import { arrayToTree } from "../utils";
import config from "../site.config";
import "./Menu.less";
import { ThemeContext } from "./ThemeContext";
import { MobileContext } from "./MobileContext";
import { MenuItem } from "../types";

export function useCurrentMenu() {
  const router = useRouter();
  const currentMenu = config.menus.find(_ => _.route === router.pathname);
  return { router, currentMenu };
}

function MenuIcon({ icon }) {
  return (
    <span className="menu-icon">
      <Icon icon={icon} />
    </span>
  );
}

const { SubMenu } = Menu;

type MenuNode = MenuItem & {
  children?: MenuNode[];
};

function renderMenus(data: MenuNode[], collapsed: boolean) {
  return data.map(item => {
    if (item.children) {
      return (
        <SubMenu
          key={item.id}
          title={
            <Fragment>
              {item.icon && <MenuIcon icon={item.icon} />}
              {collapsed ? undefined : <span>{item.name}</span>}
            </Fragment>
          }
        >
          {this.generateMenus(item.children)}
        </SubMenu>
      );
    }
    return (
      <Menu.Item key={item.id}>
        <Link href={item.route}>
          <a href={item.route}>
            {item.icon && <MenuIcon icon={item.icon} />}
            {collapsed ? undefined : <span>{item.name}</span>}
          </a>
        </Link>
      </Menu.Item>
    );
  });
}

type SiderMenuProps = {
  collapsed: boolean;
  onCollapseChange(collapsed: boolean): void;
};

// TODO persistence of last open menu
function SiderMenu({ collapsed, onCollapseChange }: SiderMenuProps) {
  const { theme } = useContext(ThemeContext);
  const { isMobile } = useContext(MobileContext);
  const { currentMenu } = useCurrentMenu();
  const [openKeys, setOpenKeys] = useState();

  const onOpenChange = openKeys => {
    const menus = config.menus;
    const rootSubmenuKeys = menus.map(_ => _.id);
    const latestOpenKey = openKeys.find(key => openKeys.indexOf(key) === -1);
    let newOpenKeys = openKeys;
    if (rootSubmenuKeys.indexOf(latestOpenKey) !== -1) {
      newOpenKeys = latestOpenKey ? [latestOpenKey] : [];
    }
    setOpenKeys(newOpenKeys);
  };

  const menuTree = arrayToTree(config.menus, "id", "menuParentId");
  const selectedKeys = currentMenu ? [currentMenu.id] : [];
  const menuProps = collapsed ? {} : { openKeys };

  return (
    <Menu
      mode="inline"
      theme={theme}
      onOpenChange={onOpenChange}
      selectedKeys={selectedKeys}
      onClick={
        isMobile
          ? () => {
              onCollapseChange(true);
            }
          : undefined
      }
      {...menuProps}
    >
      {renderMenus(menuTree, collapsed)}
    </Menu>
  );
}

export default SiderMenu;
