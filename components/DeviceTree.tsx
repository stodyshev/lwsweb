import { Tree } from "antd";
import useSWR from "swr";
import DeviceNode from "./DeviceNode";
import { fetchDevices } from "../api";

type Props = {
  except?: string;
  noSettings?: boolean;
};

export default function DeviceTree({ except, noSettings }: Props) {
  const { data, error } = useSWR("/devices", fetchDevices);

  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  const nodes = data
    .filter(d => d.name !== except)
    .map(d => DeviceNode(d, noSettings));

  const onSelect = (selectedKeys, info) => {
    console.log(selectedKeys, info);
  };

  return (
    <Tree
      className="dev-tree"
      showLine
      showIcon
      checkable={false}
      selectable
      defaultExpandAll
      onSelect={onSelect}
    >
      {nodes}
    </Tree>
  );
}
