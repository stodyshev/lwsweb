import { Tree } from "antd";
import Icon from "./Icon";

const { TreeNode } = Tree;

function NodeIcon({ icon }) {
  return (
    <span className="icon-container">
      <Icon icon={icon} />
    </span>
  );
}

export default function Node({ key, title, icon, children = undefined }) {
  return (
    <TreeNode key={key} title={title} icon={<NodeIcon icon={icon} />}>
      {children}
    </TreeNode>
  );
}
