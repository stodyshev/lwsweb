import React, { useState, useEffect } from "react";
import { enquireScreen, unenquireScreen } from "enquire-js";

export const MobileContext = React.createContext({
  isMobile: false
});

export function MobileProvider({ children }) {
  const [isMobile, setMobile] = useState(false);
  useEffect(() => {
    const enquireHandler = enquireScreen(mobile => {
      if (isMobile !== mobile) {
        setMobile(mobile);
      }
    });
    return () => unenquireScreen(enquireHandler);
  });
  return (
    <MobileContext.Provider value={{ isMobile }}>
      {children}
    </MobileContext.Provider>
  );
}
