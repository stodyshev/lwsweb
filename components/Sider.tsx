import React, { useContext } from "react";
import { Switch, Layout } from "antd";
import { ThemeContext } from "./ThemeContext";
import { MobileContext } from "./MobileContext";
import ScrollBar from "./ScrollBar";
import SiderMenu from "./Menu";
import Icon from "./Icon";
import icons from "./icons";
import config from "../site.config";
import "./Sider.less";

type SiderProps = {
  collapsed: boolean;
  onCollapseChange: (collapsed: boolean) => void;
};

function Sider(props: SiderProps) {
  const { theme, toggleTheme } = useContext(ThemeContext);
  const { isMobile } = useContext(MobileContext);
  const { collapsed, onCollapseChange } = props;

  return (
    <Layout.Sider
      width={256}
      theme={theme}
      breakpoint="lg"
      trigger={null}
      collapsible
      collapsed={collapsed}
      onBreakpoint={!isMobile && onCollapseChange}
      className={"sider"}
    >
      <div className={"brand"}>
        <div className={"logo"}>
          <img alt="logo" src={config.logoPath} />
          {!collapsed && <h1>{config.siteName}</h1>}
        </div>
      </div>

      <div className={"menuContainer"}>
        <ScrollBar
          options={{
            // Disabled horizontal scrolling, https://github.com/utatti/perfect-scrollbar#options
            suppressScrollX: true
          }}
        >
          <SiderMenu
            collapsed={collapsed}
            onCollapseChange={onCollapseChange}
          />
        </ScrollBar>
      </div>
      {!collapsed && (
        <div className={"switchTheme"}>
          <span className="inline-flex-center">
            <Icon icon={icons.bulb} size="small" />
            <span>Switch Theme</span>
          </span>
          <Switch
            onChange={toggleTheme}
            defaultChecked={theme === "dark"}
            checkedChildren={`Dark`}
            unCheckedChildren={`Light`}
          />
        </div>
      )}
    </Layout.Sider>
  );
}

export default Sider;
