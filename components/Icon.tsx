import { Icon } from "@iconify/react";

type Size = "small" | "medium" | "large";
type IconProps = {
  icon: any;
  size?: Size;
};

function toPixels(size: Size) {
  switch (size) {
    case "small":
      return 16;
    case "medium":
      return 24;
    case "large":
      return 32;
    default:
      return 24;
  }
}

export default function IconEx({ icon, size = "medium" }: IconProps) {
  const px = toPixels(size);
  return <Icon icon={icon} width={px} height={px} />;
}
