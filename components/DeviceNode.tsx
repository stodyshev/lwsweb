import _ from "lodash";
import Node from "./Node";
import { Device, DeviceInterface, InterfaceLink } from "../types";
import icons, { iconForDevice } from "./icons";

export default function DeviceNode(device: Device, noSettings = false) {
  const interfaces = _.isEmpty(device.deviceInterfaces)
    ? undefined
    : Node({
        key: `interfaces-${device.name}`,
        title: "Device Interfaces",
        icon: icons.interfaceList,
        children: _.map(device.deviceInterfaces, i =>
          DeviceInterfaceNode(i, device.name)
        )
      });
  const settings =
    device.settings && !noSettings
      ? Node({
          key: `settings-${device.name}`,
          title: "Settings",
          icon: icons.settings
        })
      : undefined;
  return Node({
    key: `device-${device.name}`,
    title: device.name,
    icon: iconForDevice(device),
    children: [interfaces, settings].filter(_.identity)
  });
}

function DeviceInterfaceNode(di: DeviceInterface, deviceName: string) {
  const link = di.link ? ` (${di.link.device}->${di.link.interface})` : "";
  return Node({
    key: `interface-${deviceName}-${di.name}`,
    title: di.name + link,
    icon: icons.interface,
    children: [LinkNode(di.link, `link-${di.name}`)]
  });
}

function LinkNode(link: InterfaceLink, key: string) {
  if (!link) {
    return undefined;
  }
  return Node({
    key,
    title: `${link.device}->${link.interface}`,
    icon: icons.link
  });
}
