import _ from "lodash";
import { Configuration } from "../types";
import Node from "./Node";
import DeviceNode from "./DeviceNode";
import icons from "./icons";

export default function ConfigNode(config: Configuration) {
  return Node({
    key: `config-${config.name}`,
    title: config.name,
    icon: icons.config,
    children: _.map(config.devices, DeviceNode)
  });
}
