import _ from "lodash";
import React, { useState, useContext } from "react";
import { Drawer, Layout, BackTop } from "antd";
import Header from "./Header";
import Footer from "./Footer";
import Sider from "./Sider";
import { MobileContext } from "./MobileContext";
import Bread from "./Bread";
import "./Page.less";

function SiderHost({ collapsed, onCollapseChange }) {
  const { isMobile } = useContext(MobileContext);
  const siderProps = {
    menus: [],
    collapsed,
    onCollapseChange
  };

  return isMobile ? (
    <Drawer
      maskClosable
      closable={false}
      onClose={() => onCollapseChange(!collapsed)}
      visible={!collapsed}
      placement="left"
      width={200}
      style={{
        padding: 0,
        height: "100vh"
      }}
    >
      <Sider {...siderProps} collapsed={false} />
    </Drawer>
  ) : (
    <Sider {...siderProps} />
  );
}

export default function Page({ children }) {
  const [collapsed, onCollapseChange] = useState(false);

  return (
    <Layout>
      <SiderHost collapsed={collapsed} onCollapseChange={onCollapseChange} />
      <div id="primaryLayout" className="container">
        <Header
          collapsed={collapsed}
          onCollapseChange={onCollapseChange}
          onSignOut={_.noop}
        />
        <div className="content">
          <Bread />
          {children}
        </div>
        <BackTop
          className={"backTop"}
          target={() => document.getElementById("primaryLayout")}
        />
        <Footer />
      </div>
    </Layout>
  );
}
