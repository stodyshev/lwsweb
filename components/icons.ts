import iconConfig from "@iconify/icons-ant-design/setting-twotone";
import iconDevice from "@iconify/icons-feather/cpu";
import iconInterface from "@iconify/icons-ant-design/api-twotone";
import iconLink from "@iconify/icons-ant-design/link-outlined";
import iconInterfaceList from "@iconify/icons-ic/outline-settings-input-component";
import iconSettings from "@iconify/icons-ic/outline-settings-ethernet";
import cpu32Bit from "@iconify/icons-mdi/cpu-32-bit";
import cpu64Bit from "@iconify/icons-mdi/cpu-64-bit";
import cpuIcon from "@iconify/icons-whh/cpu";
import serverNetworkAlt from "@iconify/icons-uil/server-network-alt";
import dashboardOutlined from "@iconify/icons-ant-design/dashboard-outlined";
import infoCircleOutlined from "@iconify/icons-ant-design/info-circle-outlined";
import bulbOutlined from "@iconify/icons-ant-design/bulb-outlined";
import menuFoldOutlined from "@iconify/icons-ant-design/menu-fold-outlined";
import menuUnfoldOutlined from "@iconify/icons-ant-design/menu-unfold-outlined";

import { Device } from "../types";

const deviceIcons = [iconDevice, cpu32Bit, cpu64Bit, cpuIcon];

export default {
  config: iconConfig,
  device: iconDevice,
  interface: iconInterface,
  interfaceList: iconInterfaceList,
  link: serverNetworkAlt,
  settings: iconSettings,
  dashboard: dashboardOutlined,
  info: infoCircleOutlined,
  bulb: bulbOutlined,
  menu: {
    fold: menuFoldOutlined,
    unfold: menuUnfoldOutlined
  }
};

let nextDeviceIcon__: number = 0;

export function iconForDevice(device: Device) {
  if (nextDeviceIcon__ >= deviceIcons.length) {
    nextDeviceIcon__ = 0;
  }
  return deviceIcons[nextDeviceIcon__++];
}
