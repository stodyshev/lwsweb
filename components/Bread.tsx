import _ from "lodash";
import React, { Fragment } from "react";
import { Breadcrumb } from "antd";
import Link from "next/link";
import Icon from "./Icon";
import { MenuItem } from "../types";
import { useCurrentMenu } from "./Menu";
import config from "../site.config";
import "./Bread.less";

function BreadIcon({ icon }) {
  return (
    <span className="bread-icon">
      <Icon icon={icon} />
    </span>
  );
}

function renderBreadcrumbs(items: MenuItem[]) {
  return items.map((item, key) => {
    const content = (
      <a href={item.route}>
        {item.icon && <BreadIcon icon={item.icon} />}
        {item.name}
      </a>
    );

    return (
      item && (
        <Breadcrumb.Item key={key}>
          <Link href={item.route}>{content}</Link>
        </Breadcrumb.Item>
      )
    );
  });
}

function Bread() {
  const { currentMenu } = useCurrentMenu();
  const dashboard = config.menus.find(t => t.id === "/");
  const items = _.uniq([dashboard, currentMenu].filter(_.identity));

  return <Breadcrumb className="bread">{renderBreadcrumbs(items)}</Breadcrumb>;
}

export default Bread;
