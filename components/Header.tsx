import React, { PureComponent, Fragment } from "react";
import { Menu, Layout, Avatar } from "antd";
import classnames from "classnames";
import Icon from "./Icon";
import icons from "./icons";
import config from "../site.config";
import "./Header.less";

const { SubMenu } = Menu;

type HeaderProps = {
  collapsed: boolean;
  onSignOut(): void;
  onCollapseChange(collapsed: boolean);
};

class Header extends PureComponent<HeaderProps, {}> {
  handleClickMenu = e => {
    e.key === "SignOut" && this.props.onSignOut();
  };
  render() {
    const { collapsed, onCollapseChange } = this.props;

    // FIXME dynamic user and avatar
    const username = "buddy";
    const avatar =
      "//image.zuiidea.com/photo-1525879000488-bff3b1c387cf.jpeg?imageView2/1/w/200/h/200/format/webp/q/75|imageslim";

    const rightContent = [
      <Menu key="user" mode="horizontal" onClick={this.handleClickMenu}>
        <SubMenu
          title={
            <Fragment>
              <span style={{ color: "#999", marginRight: 4 }}>Hi,</span>
              <span>{username}</span>
              <Avatar style={{ marginLeft: 8 }} src={avatar} />
            </Fragment>
          }
        >
          <Menu.Item key="SignOut">Sign out</Menu.Item>
        </SubMenu>
      </Menu>
    ];

    return (
      <Layout.Header
        className={classnames("header", {
          fixed: config.fixedHeader,
          collapsed: collapsed
        })}
        id="layoutHeader"
      >
        <div
          className={"button"}
          onClick={onCollapseChange.bind(this, !collapsed)}
        >
          <Icon icon={collapsed ? icons.menu.unfold : icons.menu.fold} />
        </div>

        <div className={"rightContainer"}>{rightContent}</div>
      </Layout.Header>
    );
  }
}

export default Header;
