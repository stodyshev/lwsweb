import React from "react";
import { useLocalStorage } from "crooks";
import { Theme } from "../types";

export type ThemeContextType = {
  theme: Theme;
  toggleTheme(): void;
};

export const ThemeContext = React.createContext<ThemeContextType>({
  theme: "light",
  toggleTheme() {}
});

export function ThemeProvider({ children }) {
  const [theme, setTheme] = useLocalStorage("theme", "light");

  const toggleTheme = () => {
    setTheme(theme === "dark" ? "light" : "dark");
  };

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
}
